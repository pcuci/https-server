const https = require('https');
const fs = require('fs');

const options = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};


console.log('listening...')

https.createServer(options, function (req, res) {
  console.log('responding...')
  res.writeHead(200);
  res.end("nodejs https server\n");
}).listen(3000);